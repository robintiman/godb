package compile

import (
	"github.com/godb/engine"
	"log"
	"strconv"
	"strings"
)

func Query(input string, db *engine.BTree) {
	// TODO These are basically identical. Consider refactoring
	parts := strings.Split(input, " ")
	if parts[0] == "select" {
		key, err := strconv.Atoi(parts[1])
		if err != nil {
			log.Fatal("Key not an int")
		}
		db.Find(key)
	}
	if parts[0] == "insert" {
		keyString, value := parts[1], parts[2]
		key, err := strconv.Atoi(keyString)
		if err != nil {
			log.Fatal("Key not an int")
		}
		db.Insert(key, value)
	}
}
