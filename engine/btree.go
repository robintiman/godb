package engine

import (
	"fmt"
	"log"
	"sort"
)

const (
	// TODO remove the instances where this is not a constant.
	DEGREE = 3
	PAGE_SIZE              = 5
	LEAF_RIGHT_SPLIT_INDEX = (PAGE_SIZE + 1) / 2
	LEAF_LEFT_SPLIT_INDEX  = (PAGE_SIZE + 1) - LEAF_RIGHT_SPLIT_INDEX
)

type Node struct {
	Num_keys int
	Leaf     bool
	Parent   *Node
	Children []*Node
	Keys     []int
	Values   map[int]string
}

func InitNode(nodeType NodeType, degree int, parent *Node) *Node {
	// TODO refactor
	if nodeType == LEAF {
		keys := make([]int, 0, PAGE_SIZE)
		values := make(map[int]string, PAGE_SIZE)
		return &Node{Num_keys: 0, Leaf: true, Parent: parent, Children: nil,
			Keys: keys, Values: values}
	} else {
		keys := make([]int, 0, degree-1)
		Children := make([]*Node, 0, degree)
		return &Node{Num_keys: 0, Parent: parent, Children: Children,
			Keys: keys, Values: nil, Leaf: false}
	}
}

// It's root if it doesn't have a parent.
func (n *Node) isRoot() bool {
	return n.Parent == nil
}

// Recursive search function.
func (n *Node) search(key int) (*Node, bool) {
	// Find the first key greater than or equal to key
	i := 0
	for i < n.Num_keys && key > n.Keys[i] {
		i++
	}
	// If key is found. Return a pointer to the node
	if i < n.Num_keys && n.Keys[i] == key {
		return n, true
	}
	// If not found and this is a leaf; return nil
	if n.Leaf {
		return n, false
	}
	// Continue the search recursively.
	return n.Children[i].search(key)
}

func (n *Node) isFull() bool {
	return len(n.Keys) == cap(n.Keys)
}

// Method for inserting a new key value pair.
// Assumes that there is space.
func (n *Node) InsertKeyValue(newKey int, newValue string) {
	pos := sort.Search(newKey, func(x int) bool {
		return newKey > x
	})
	n.Keys = append(n.Keys, 0)
	copy(n.Keys[pos+1:], n.Keys[pos:])
	n.Keys[pos] = newKey
	n.Values[newKey] = newValue
	n.Num_keys += 1
}

func (n *Node) deleteKey(key int, index int) {
	// Assumes that the key exists
	delete(n.Values, key)
	n.Num_keys -= 1
	n.Keys = append(n.Keys[:index], n.Keys[index+1:]...)
}

// If there is no space on the leaf node, we would split the existing entries residing
// there and the new one (being inserted) into two equal halves: lower and upper halves.(Keys on
// the upper half are strictly greater than those on the lower half.) We allocate a new leaf node,
// and move the upper half into the new node.
func (n *Node) splitNode(newKey int, newValue string) *Node {
	// Assumes that node n is full and needs to be split.
	newSibling := InitNode(LEAF, cap(n.Keys), n.Parent)
	// Insert values into the new node.
	median := Median(n.Keys)
	if newKey > median {
		newSibling.InsertKeyValue(newKey, newValue)
	} else {
		n.InsertKeyValue(newKey, newValue)
	}
	// Move the upper half of the keys to the new node.
	for i := len(n.Keys) - 1; i >= LEAF_LEFT_SPLIT_INDEX; i-- {
		// Move into new node
		key := n.Keys[i]
		newSibling.InsertKeyValue(key, n.Values[key])
		n.deleteKey(n.Keys[i], i)
	}
	return newSibling
}

// Let N be the root node. First allocate two nodes, say L and R. Move lower half of N into L and
// the upper half into R. Now N is empty. Add 〈L, K,R〉 in N, where K is the max key in L. Page N
// remains the root. Note that the depth of the tree has increased by one, but the new tree
// remains height balanced without violating any B+-tree property.
func createNewRoot(children []*Node) *Node {
	log.Fatalln("No new root")
	return InitNode(INTERNAL, DEGREE, nil)
}

// BTree struct of the given degree (m)
// Requirements:
//	* Up to m children per internal node.
//	* Up to m-1 keys per internal node.
//	* At least m-1 children per internal node.
//	* At least m-2 keys per internal node.
type BTree struct {
	Degree   int
	Num_keys int
	Root     *Node
}

func InitBTree(degree int) BTree {
	btree := BTree{Degree: degree, Num_keys: 0, Root: nil}
	return btree
}

func (tree *BTree) Insert(newKey int, newValue string) {
	root := tree.Root
	if root == nil {
		root = InitNode(NodeType(LEAF), tree.Degree, nil)
		root.InsertKeyValue(newKey, newValue)
		tree.Root = root
		return
	}

	// Search for the position to store the new key/newValue.
	node, keyExists := root.search(newKey)
	if keyExists {
		fmt.Println("Key already exists")
		return
	}

	// If the leaf node is full we need to split it to make room for the new data pair.
	if node.isFull() {
		newSibling := node.splitNode(newKey, newValue)
		// Need to update the parent.
		if node.isRoot() {
			// We need to create a new node because the root can't have any siblings.
			children := []*Node{node, newSibling}
			newRoot := createNewRoot(children)
			tree.Root = newRoot
		} else {
			log.Fatal("Updating parent not implemented.")
		}
		return
	}

	node.InsertKeyValue(newKey, newValue)
}

func (tree *BTree) Find(key int) {
	node, found := tree.Root.search(key)
	if found {
		fmt.Println(node.Values[key])
	} else {
		fmt.Println("Node not found")
	}
}

func (tree BTree) Delete() {

}
