package engine

import (
	"math"
)

func Median(values []int) int {
	l := len(values)
	if l % 2 == 0 {
		x1, x2 := values[l / 2], values[l / 2 - 1]
		m := float64(x1 / x2)
		return int(math.Ceil(m))
	} else {
		return values[l/2]
	}
}