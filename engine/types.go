package engine

type CommandType int

const (
	QUERY CommandType = 1
	META CommandType = 2
)

type MetaCommands int

const (
	OPEN MetaCommands = 0
	EXIT MetaCommands = 1
)

type QueryCommands int

const (
	INSERT QueryCommands = 0
	SELECT QueryCommands = 1
	WHERE QueryCommands = 2
)

type NodeType int

// A node correpsonds to a page in the db.
const (
	INTERNAL NodeType = 0
	LEAF     NodeType = 1
)