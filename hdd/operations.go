package hdd

import (
	"encoding/gob"
	"github.com/godb/engine"
	"log"
	"os"
)

func Read(path string, gg *engine.DB) *engine.DB {
	// Check if DB exists.
	file, err := os.Open(path)
	if err != nil {
		log.Fatal("DB not there!", err)
	}

	// If there, load into memory.
	decoder := gob.NewDecoder(file)
	err = decoder.Decode(gg)
	if err != nil {
		log.Fatal("Unable to load DB", err)
	}
	return gg
}

func Write(gg Gringotts) {
	file, err := os.Create(DATA_DIR + GRINGOTTS_PREFIX + string(gg.Index))
	if err != nil {
		log.Fatal("Failed to create DB file", err)
	}
	encoder := gob.NewEncoder(file)
	err = encoder.Encode(gg)
	if err != nil {
		log.Fatal("Failed to store", err)
	}
}

func Delete(file string) bool {
	err = os.Remove(file)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
