package main

import (
	"bufio"
	"fmt"
	"github.com/godb/compile"
	"github.com/godb/engine"
	"github.com/godb/shell"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	db := engine.InitBTree(3)
	fmt.Println("Welcome to GoDB!")
	// Main loop
	for i := 1; i < 10; i++ {
		input := shell.WaitForInput(reader)
		input = fmt.Sprintf("insert %d Robin", i)
		fmt.Println(input)
		switch shell.Interpret(input) {
		case engine.QUERY:
			compile.Query(input, &db)
		case engine.META:
			compile.Meta(input)
		default:
			fmt.Println("Unrecognized command")
		}
	}
	fmt.Println("Goodbye!")
}
