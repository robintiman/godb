package shell

import (
	"bufio"
	"fmt"
	"github.com/godb/engine"
	"strings"
)

func WaitForInput(reader *bufio.Reader) string {
	fmt.Print(">> ")
	input, _ := reader.ReadString('\n')
	input = strings.ToLower(input)
	input = input[:len(input)-1]
	return input
}

func Interpret(input string) engine.CommandType {
	if input[0] == '.' {
		return engine.CommandType(engine.META)
	} else {
		return engine.CommandType(engine.QUERY)
	}
}